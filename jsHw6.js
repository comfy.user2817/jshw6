

"use strict"



//1



//Створюю об"єкт

const product = {
    name: '',
    price: 0,
    discount: 0,
    
    //Створюю метод для вводу ім"я

    sayName() {
        this.name = prompt('Введіть ім`я:');

        //Виводжу ім"я на екран

        return this.name;
    },

    //Створюю метод для вводу ціни

    sayPrice() {
        
        //Зациклюю для зручної валідації

        for(;;) {
            
            //Запрошую значення через промпт

            let price = +prompt(`Назвіть ціну для ${this.name}:`);
            
            //Перевіряю чи є ціна числом

            if( (parseInt(price) == NaN) ) {
                alert(`"${price}" не є числом. Спробуйте ще`)
                continue
            }
            
            //Перевіряю чи не є ціна нульовою або відмінною

            else if (price <= 0) {
                alert(`Ціна не має бути менше, або дорівнювати нулю`)
                continue
            }
            
            //Валідацію пройдено, ціна зазначається як властвість товару

            else {
                this.price = price;
                break
            }

        }
        
        //У випадку успіху виводить значення ціни на екран

        return this.price;
    },
    
    //Створюю метод для вводу знижки

    sayDiscount() {
        
        //Також зациклюю щоб валідувати значення

        for(;;){
            
            //Запрошую значення через промпт і проводжу всі ті самі перевірки

            let disc = +prompt(`Вкажіть знижку (у відстотках) для товару "${this.name}" з ціною ${this.price}`)

            if( (parseInt(disc) == NaN)) {
                alert(`"${disc} не є числом. Спробуйте ще"`);
                continue
            }
            else if (disc <= 0) {
                alert(`Знижка не має бути менше, або дорівнювати нулю`)
                continue
            }
            
            //Також перевіряю щоб знижка не була нижча за 80%

            else if (disc > 80) {
                alert(`${disc}% це за надто багато. Вкажіть знижку менше за 80%`)
                continue
            }
            else {
                
                //Валідацію пройдено, знижка зазначається як властвість товару

                this.discount = disc;
                break
            }
        }
        
        //У випадку успіху виводить значення ціни на екран

        return this.discount;
    },
    
    //Створюю метод для виведення нової ціни

    isTruePrice() {
        let truePrice = this.price - (this.price * (this.discount / 100));
        this.newPrice = truePrice;
        
        //Метод сам виводить значення на екран

        return console.log(`"${this.name}" коштує ${truePrice} з урахуванням знижки ${this.discount}%`)
        
    },

//Сворюю метод що запустить всі методи почерзі

    goProduct() {
        this.sayName();
        this.sayPrice();
        this.sayDiscount();
        this.isTruePrice();
    },

}


//Викликаю методи

//product.sayName();
//product.sayPrice();
//product.sayDiscount();
//product.isTruePrice();

//Або

//product.goProduct();







//1 варіант 2






//Або можно просто зробити так

//Створюю об"єкт

const productTwo = {
    
    truePrice(price,disc) {

            //Метод сам зазначає ім"я
    
            this.name = 'Mivina';
    
            //При виклику метода значення ціни і знижки автоматично
    
            this.price = price;
            this.discount = disc;
    
            //Валідуємо знижку щоб вона не була за надто високою. Якщо все вірно, тоді нова ціна записується як властивість товару
    
            disc > 80 ? alert(`${disc}% це за надто багато. Вкажіть знижку менше за 80%`) : this.newPrice = price - (price * (disc / 100));
    
            //Повертаємо значення ціни з урахуванням знижки
    
            return console.log(`New price = ${this.newPrice}`);
        }
    
        
}
//Викликаю метод
//productTwo.truePrice(price,disc)








//2




//Створюю об"єкт

let object = {

    //Створюю функцію в ньому

    hi() {

    //Запрошую ім"я

    let name = prompt(`Введіть своє ім"я:`);

    //Запрошую вік

    let age = prompt(`${name}, введіть ваш вік:`);

    //Задаю отриманні значенння як властивості об"єкту

    this.name = name;
    this.age = age;

    //Виводжу на екран привітання

    return [alert(`Првіт! Я ${name}, мені ${age} років!`), console.log(`Првіт! Я ${name}, мені ${age} років!`)]

    }

}
//Викликаю метод
//object.hi();



//3


const clone = structuredClone(product);



